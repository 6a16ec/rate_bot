# Rate Bot

#### Telegram Bot for team project evaluation.

* [Getting started](#getting-started)
 * [Pre-requirements](#pre-requirements)
 * [Installation](#installation)
 * [Configuration](#configuration)
 * [Launching](#lauching)
* [How to use](#how-to-use)
 * [Authentification](#authentification)
 * [Working with media](#working-with-media)
 * [Settings](#settings)

### Getting Started

#### Pre-requirement
* You should have [docker](https://docs.docker.com/install) and [docker-compose] (https://docs.docker.com/compose/install/) installed on your server. 

* In order to launch the bot, you need pre-configured PostgreSQL database see`rate-bot-web`. See [Configuration](#configuration) section to setup database connection from the bot.

Tested on `Ubuntu 16.04 LTS`, `Docker 17.12.0` and `CPython 3.6.2`.

#### Installation
Installation from source (requires git):

```
git clone https://gitlab.com/inno-olymp-bots/rate_bot.git
cd rate_bot
```

#### Configuration
All the settings should be in the `settings.py` file that is inside `core` directory. 

##### BOT

Fields that should be in the `BOT` section:

```
# BOT
# Telegram token of your bot from @BotFather
TOKEN = "123456789:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"

# Default language for all of the users 
DEF_LANG = "ru"

# Amount of time that determines the period for mark modification
MIN_TO_MODIFY = 10
```
Users would be able to change language in the [Settings](#settings) menu. Admin can add new languages via web interface. See `rate-bot-web` for more info how to it.

##### PROXY

Fields that should be in the `PROXY` section:

```
# PROXY
# If you want to use proxy for your bot, please set USE_PROXY to 'yes'
USE_PROXY = False
```
**Optional params:**

```
addr = 10.10.10.10
port = 7713
username = username
password = password
```

##### RMQ

Fields that should be in the `RMQ` section:

```
# RMQ
HOST = rmq
```
Insert your name of RabbitMQ container

##### PSG

Fields that should be in the `PostgreSQL` section:

```
# PSG
dbname = rate_bot
user = username
password = password
host = 100.10.100.10
port = 5432
```

**Starting**

```
bash start-prod-server.sh
```

### How to use

#### Authentification
All judges should use tokens that was created by admin via `rate-bot-web`. 

_Each token assigned to only one judge. In case if two users would try to use the same token, only the last one who logged in would be able to work with bot._

#### Working with media
During the evaluation judges can send next types of the media files:

* Photo
* Voice
* Video

This media files could be retrieved from the project card. 

_Only one media file of each type could be saved for each project._

#### Settings
Each judge can change their language and rate scale settings. Languages could be added to the bot via `rate-bot-web` interface. However, there are only 2 types of scale `0..2` and `0..5`

_In case of re-login from a different Telegram Account all of the settings would be saved._



## LABELS
Bot supports [Telegram Markdown](https://core.telegram.org/bots/api#markdown-style)

\***Bold Text***

\__Italic Text__

{} - this is a formatting bracket

| Label Name | Description | Example |
| -------- | -------- | ------- |
|lang_code| Language code in 2 letter format (lower case)| **ru** |
|msg_start| Welcome message for users | *Welcome* to WRO Rate Bot. Send your token to sign in: |
|msg\_token_request| Token Request |*Your token:*|
|msg\_wrong_token| Unable to find this token |Sorry, can’t find this token. *Please, try again:* |
|msg_welcome| {username}, {Language name} |You logged in as \*{}\*  \n\nDefault settings:\nRate Scale: \*0..2\*  \nLanguage: \*{}*|
||||
||||
||||
||||
