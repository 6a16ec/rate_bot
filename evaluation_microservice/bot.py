#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import re
import pickle
import logging

from telebot import TeleBot, apihelper, logger

from core import database as db
from core import markup as mrk
from core import settings as s
from core.consumer import PickleConsumer

if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    if body["command"] and body["command"] == "resend_evaluation":
        resend_evaluation(body["message"].from_user.id , by_last_visited_criterion=True, media=True)
    elif body["call"]:
        bot.process_new_callback_query([body["call"]])
    else:
        bot.process_new_messages([body["message"]])


con = PickleConsumer(s.RMQ, 5672, 'evaluation', callback, exchange='rate_bot', logger='evaluation', username=s.RMQ_USER, password=s.RMQ_PASS)

@bot.message_handler(content_types=["text"])
def text_handler(message):
    judge_id = message.from_user.id
    resend_evaluation(judge_id)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    judge_id = call.from_user.id
    command = re.search('(?<=/)\w+', call.data).group(0)
    if command == "c_ce":
        bot.answer_callback_query(call.id)
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        db.activate_evaluation_mode(judge_id, project_id)
        last_msg = db.fetch_last_message(judge_id)
        send_evaluate_message(judge_id, 0)
        bot.delete_message(judge_id, last_msg)
    elif command == "c_cm":
        bot.answer_callback_query(call.id)
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        db.set_modified(judge_id, project_id)
        db.activate_evaluation_mode(judge_id, project_id)
        last_msg = db.fetch_last_message(judge_id)
        send_evaluate_message(judge_id, 0)
        bot.delete_message(judge_id, last_msg)
    elif command == "c_en":
        bot.answer_callback_query(call.id)
        criterion_number = int(re.search('(?<=-c.)\w+', call.data).group(0))
        last_msg = db.fetch_last_message(judge_id)
        db.update_last_criterion(judge_id=judge_id, new_last_criterion=criterion_number)
        send_evaluate_message(judge_id, criterion_number, last_msg_id=last_msg)
    elif command == "c_ep":
        bot.answer_callback_query(call.id)
        criterion_number = int(re.search('(?<=-c.)\w+', call.data).group(0))
        last_msg = db.fetch_last_message(judge_id)
        db.update_last_criterion(judge_id=judge_id, new_last_criterion=criterion_number)
        send_evaluate_message(judge_id, criterion_number, last_msg_id=last_msg)
    elif command == "c_ssc":
        bot.answer_callback_query(call.id, text="\U0001F4BE")
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        score = int(re.search('(?<=-s.)\w+', call.data).group(0))
        criterion_number = int(re.search('(?<=-c.)\w+', call.data).group(0))
        db.set_mark(judge_id, project_id, score)
        last_msg = db.fetch_last_message(judge_id)
        send_evaluate_message(judge_id, criterion_number, last_msg_id=last_msg)
        db.update_last_criterion(judge_id, criterion_number)
    elif command == "c_fe":
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        if db.all_marks_set(judge_id, project_id):
            lang = db.get_usr_lang(judge_id)
            project_title = db.get_project_title_by_id(project_id)
            msg_you_have_finished = db.fetch_labels(lang, "msg_you_have_finished")[0]
            last_msg = db.fetch_last_message(judge_id)
            bot.delete_message(judge_id, last_msg)
            bot.send_message(judge_id, text=msg_you_have_finished.format(project_title))
            db.confirm_finish(judge_id, project_id)
            notify_menu_service(call)
        else:
            lang = db.get_usr_lang(judge_id)
            msg_you_need_to_finish = db.fetch_labels(lang, "msg_you_need_to_finish")
            bot.answer_callback_query(call.id, text=msg_you_need_to_finish, show_alert=True)
            first_crit = db.fetch_first_unset_criteria_id(judge_id, project_id)
            last_msg = db.fetch_last_message(judge_id)
            db.update_last_criterion(judge_id, first_crit)
            send_evaluate_message(judge_id, first_crit, last_msg_id=last_msg)


def send_evaluate_message(judge_id, criterion_number, last_msg_id=None, project_id=None):
    lang = db.get_usr_lang(judge_id)
    menu_buttons, criterion_count = db.fetch_criterion_by_id(judge_id, criterion_number)
    read_only = menu_buttons[1][5]
    current_mark = db.get_mark(judge_id)
    team_name, title, evaluate_team_markup, description, criterion_count, criterion_title = \
        mrk.evaluate_team_inline_keyboard(judge_id, criterion_number, read_only=read_only)
    msg_current_mark, msg_none = db.fetch_labels(lang, "msg_current_mark", "msg_none")
    msg_text = "*{}\n{}\n{} ({}/{})*\n\n{}\n\n*{}:* `{}`".format(
        team_name,
        criterion_title,
        title,
        criterion_number + 1,
        criterion_count,
        description if description is not None else msg_none,
        msg_current_mark,
        current_mark if read_only else int(current_mark) if current_mark is not None else msg_none
    )
    if last_msg_id is None:
        sent_message = bot.send_message(
            text=msg_text,
            chat_id=judge_id,
            parse_mode="markdown",
            reply_markup=evaluate_team_markup
        )
        db.update_last_message(judge_id, sent_message.message_id)
    else:
        bot.edit_message_text(
            text=msg_text,
            chat_id=judge_id,
            message_id=last_msg_id,
            reply_markup=evaluate_team_markup,
            parse_mode="markdown"
        )


def resend_evaluation(judge_id, by_last_visited_criterion=False, media=False):
    lang = db.get_usr_lang(judge_id)
    if media:
        msg_you_need_to_finish = db.fetch_labels(lang, "msg_your_media_was_saved")
    else:
        msg_you_need_to_finish = db.fetch_labels(lang, "msg_you_need_to_finish")
    bot.send_message(judge_id, text=msg_you_need_to_finish, reply_markup=mrk.MAIN_MENU_MARKUP)

    project_id = db.get_current_state_prefix(judge_id)
    if by_last_visited_criterion:
        criterion_id = db.fetch_last_criterion_id(judge_id)
    else:
        criterion_id = db.fetch_first_unset_criteria_id(judge_id, project_id)
        db.update_last_criterion(judge_id, criterion_id)
    last_msg = db.fetch_last_message(judge_id)
    send_evaluate_message(judge_id, criterion_id)
    bot.delete_message(judge_id, last_msg)


def notify_menu_service(call):
    body = pickle.dumps(
        {
            "command": "send_team_select",
            "message": None,
            "call": call
        }
    )
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="main_menu",
        body=body
    )


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[EVALUATION] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()

