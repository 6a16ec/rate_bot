#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import re
import pickle
import logging

from telebot import TeleBot, apihelper, logger

from core import database as db
from core import markup as mrk
from core import settings as s
from core.consumer import PickleConsumer

if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    """
        Message receiver
        Inner commands:
        'show_archive' - requests archive for the team

    :param consumer:
    :param body:
    :return:
    """
    if body["command"] and "show_archive" in body["command"]:
        team_id = int(re.search('(?<=-t.)\w+', body["command"]).group(0))
        db.update_last_criterion(body["call"].from_user.id, 0)
        db.set_state_prefix(body["call"].from_user.id, team_id)
        db.set_state(body["call"].from_user.id, s.State.ARCHIVE.value)
        send_archive_message(body["call"].from_user.id, 0)
    elif body["command"] == "send_project_card":
        judge_id = body["call"].from_user.id
        project_id, round_id = body["args"]
        db.set_state(judge_id, new_state=s.State.ARCHIVE.value)
        db.set_state_prefix(judge_id, project_id)
        db.set_current_round(judge_id, round_id)
        send_project_card(judge_id, project_id, round_id=round_id)
    elif body["call"]:
        bot.process_new_callback_query([body["call"]])
    else:
        bot.process_new_messages([body["message"]])


con = PickleConsumer(s.RMQ, 5672, 'archive', callback, exchange='rate_bot', logger='archive', username=s.RMQ_USER, password=s.RMQ_PASS)


@bot.message_handler(content_types=["text"])
def main_menu_handler(message):
    judge_id = message.from_user.id
    if message.text == mrk.B_HISTORY:
        db.set_state(judge_id, s.State.ARCHIVE.value)
        send_round_selector(judge_id)
    elif message.text == mrk.B_REFRESH:
        send_round_selector(judge_id)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    judge_id = call.from_user.id
    lang = db.get_usr_lang(judge_id)
    command = re.search('(?<=/)\w+', call.data).group(0)
    if command == "c_sr":
        bot.answer_callback_query(call.id)
        round_id = int(re.search('(?<=-n.)\w+', call.data).group(0))
        db.set_current_round(judge_id, round_id)
        send_team_select(judge_id, round_id)
    elif command == "c_sac":
        bot.answer_callback_query(call.id)
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
        db.set_state_prefix(judge_id, project_id)
        send_project_card(judge_id, project_id, round_id)
    elif command == "c_str":
        msg_team_select = db.fetch_labels(lang, "msg_team_select")[0]
        round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
        data = call.data.split("-r")[0]
        team_ids = re.findall('\d+', data)
        teams_data = db.fetch_team_names_by_ids(team_ids)
        mrk_team_response = mrk.get_detailed_team_select_keyboard(teams_data, archive=True, round_id=round_id)
        msg_text = msg_team_select.format(mrk.B_HISTORY, db.get_current_round(judge_id), "")
        bot.send_message(chat_id=judge_id,
                         text=msg_text,
                         reply_markup=mrk_team_response["markup"],
                         parse_mode="markdown")
    elif command == "c_sa":
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
        db.set_state_prefix(judge_id, project_id)
        if db.project_was_evaluated(judge_id, project_id, round_id):
            bot.answer_callback_query(call.id)
        else:
            msg_marks_are_empty = db.fetch_labels(lang, "msg_marks_are_empty")
            bot.answer_callback_query(call.id, text=msg_marks_are_empty)
        db.update_last_criterion(judge_id, 0)
        send_archive_message(judge_id, 0, round_id=round_id)
    elif command == "c_an":
        bot.answer_callback_query(call.id)
        round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
        criterion_number = int(re.search('(?<=-c.)\w+', call.data).group(0))
        last_msg = db.fetch_last_message(judge_id)
        db.update_last_criterion(judge_id=judge_id, new_last_criterion=criterion_number)
        send_archive_message(judge_id, criterion_number, last_msg_id=last_msg, round_id=round_id)
    elif command == "c_ap":
        bot.answer_callback_query(call.id)
        round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
        criterion_number = int(re.search('(?<=-c.)\w+', call.data).group(0))
        last_msg = db.fetch_last_message(judge_id)
        db.update_last_criterion(judge_id=judge_id, new_last_criterion=criterion_number)
        send_archive_message(judge_id, criterion_number, last_msg_id=last_msg, round_id=round_id)
    elif command == "c_fa":
        bot.answer_callback_query(call.id)
        last_msg = db.fetch_last_message(judge_id)
        send_round_selector(judge_id, last_msg=last_msg)


def send_project_card(user_id, project_id, round_id=None):
    lang = db.get_usr_lang(user_id)
    msg_project_card, butt_go_to_marks = db.fetch_labels(lang, "msg_project_card", "but_go_to_marks")
    project_data = db.fetch_project_card_data(project_id)
    project_card_markup = mrk.get_project_card_inline_keyboard(project_id, butt_go_to_marks,
                                                               archive=True, round_id=round_id)
    bot.send_message(chat_id=user_id,
                     text=msg_project_card.format(**project_data),
                     reply_markup=project_card_markup,
                     parse_mode="markdown")


def send_round_selector(judge_id, last_msg=False):
    lang = db.get_usr_lang(judge_id)
    rounds_markup = mrk.create_rounds_mrk(judge_id)
    msg_my_archive = db.fetch_labels(lang, "msg_select_round")[0]
    msg_my_archive = mrk.B_HISTORY + msg_my_archive
    if last_msg:
        bot.edit_message_text(chat_id=judge_id,
                              message_id=last_msg,
                              text=msg_my_archive,
                              parse_mode="markdown",
                              reply_markup=rounds_markup)
    else:
        bot.send_message(judge_id,
                         text=msg_my_archive,
                         parse_mode="markdown",
                         reply_markup=rounds_markup)


def send_archive_message(judge_id, criterion_number, last_msg_id=None, round_id=None):
    lang = db.get_usr_lang(judge_id)
    current_mark = db.get_mark(judge_id, round_id=round_id, archive=True)
    temp_data = mrk.evaluate_team_inline_keyboard(
        judge_id, criterion_number, archive=True, round_id=round_id
    )
    msg_current_mark, msg_none = db.fetch_labels(
        lang, "msg_current_mark", "msg_none"
    )
    msg_text = "*{}\n{}\n{} ({}/{})*\n\n{}\n\n*{}: {}*".format(
        temp_data[0], temp_data[5], temp_data[1], criterion_number + 1, temp_data[4],
        temp_data[3], msg_current_mark,
        current_mark if current_mark is not None else msg_none
    )
    if last_msg_id is None:
        sent_message = bot.send_message(
            text=msg_text,
            chat_id=judge_id,
            parse_mode="markdown",
            reply_markup=temp_data[2]
        )
        db.update_last_message(judge_id, sent_message.message_id)
    else:
        bot.edit_message_text(
            text=msg_text,
            chat_id=judge_id,
            message_id=last_msg_id,
            reply_markup=temp_data[2],
            parse_mode="markdown"
        )


def send_team_select(judge_id, round_id):
    lang = db.get_usr_lang(judge_id)
    msg_team_select = db.fetch_labels(lang, "msg_team_select")[0]
    teams = db.fetch_teams_for_judge_by_round(judge_id, round_id)
    if teams:
        mrk_team_response = mrk.get_detailed_team_select_keyboard(teams, archive=True, round_id=round_id)
        if mrk_team_response["small"]:
            msg_text = msg_team_select.format(mrk.B_HISTORY, db.fetch_round_title(round_id), "")
        else:
            msg_text = msg_team_select.format(mrk.B_HISTORY, db.fetch_round_title(round_id), mrk_team_response["description"])
        bot.send_message(judge_id,
                         text=msg_text,
                         parse_mode="markdown",
                         reply_markup=mrk_team_response["markup"])
    else:
        msg_no_available_teams = db.fetch_labels(lang, "msg_no_available_teams")[0]
        bot.send_message(judge_id,
                         parse_mode="markdown",
                         text=msg_no_available_teams,
                         reply_markup=mrk.MAIN_MENU_MARKUP)


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[ARCHIVE] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
