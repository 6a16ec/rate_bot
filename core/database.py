#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import datetime
import logging

import psycopg2

from core import settings

postgres_credentials = "dbname={dbname} user={user} host={host} port={port} " \
                       "password={password}".format(**settings.POSTGRESQL)
conn = psycopg2.connect(postgres_credentials)
conn_failed = 0


def _recon():
    global conn
    conn = psycopg2.connect(postgres_credentials)


def execute(request, fetch_all=False):
    global conn, conn_failed

    curs = conn.cursor()
    try:
        curs = conn.cursor()
        curs.execute(request)
        if "UPDATE" in request or "INSERT" in request or "DELETE" in request:
            conn.commit()
            return True
        elif "SELECT" in request:
            if fetch_all:
                return curs.fetchall()
            return curs.fetchone()
    except psycopg2.Error as err:
        _recon()
        conn_failed += 1
        logging.error("{}\n{}".format(err.pgcode, err))
        try:
            conn.rollback()
        except psycopg2.DatabaseError:
            _recon()
        if conn_failed >= 3:
            logging.error("3 requests")
            return None

        conn = psycopg2.connect(postgres_credentials)
        return execute(request, fetch_all=fetch_all)


"""
    LANGUAGE REQUESTS
"""


def get_usr_lang(user_id):
    """
        Returns language code if user has modified flag
        Else DEF_LANG
        :param user_id:
        :return: lang_code in a format of short language code in lower case
    """
    if lang_modified_flag(user_id):
        lang = _get_user_lang(user_id)
    else:
        lang = settings.DEF_LANG
    return lang


def get_usr_lang_id(user_id):
    """
        Returns language id if user has modified flag
        Else DEF_LANG
        :param user_id:
        :return: lang_code in a format of short language id in lower case
    """
    if lang_modified_flag(user_id):
        lang = _get_user_lang_id(user_id)
    else:
        lang = _get_default_lang_id()
    return lang


def _get_user_lang(user_id):
    request = """SELECT code 
                 FROM judges_language 
                 WHERE id 
                 IN (SELECT lang_id 
                     FROM judges_judge 
                     WHERE tel_id={})""".format(user_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def _get_user_lang_id(user_id):
    request = """SELECT lang_id 
                 FROM judges_judge 
                 WHERE tel_id={}""".format(user_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def _get_default_lang_id():
    request = """SELECT id
                 FROM judges_language
                 WHERE code='{}';""".format(settings.DEF_LANG)
    resp = execute(request)
    return resp[0] if resp is not None else None


def fetch_all_langs():
    request = """SELECT id, name
                 FROM judges_language;"""
    return execute(request, fetch_all=True)


def get_full_lang_name(lang_code):
    request = """SELECT name 
                 FROM judges_language
                 WHERE code='{}';""".format(lang_code)
    resp = execute(request)
    return resp[0] if resp is not None else None


def lang_modified_flag(user_id):
    request = """SELECT lang_modified 
                 FROM judges_judge 
                 WHERE tel_id={};""".format(user_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def fetch_labels(lang_code, *args):
    request = """SELECT {} 
                 FROM labels_labels 
                 WHERE lang_code='{}';""".format(",".join(args), lang_code)
    return execute(request)


"""
    AUTH REQUESTS
"""


def token_exists(token):
    request = """SELECT COUNT(*) 
                 FROM judges_judge 
                 WHERE token='{}';""".format(token)
    resp = execute(request)
    return resp[0] if resp is not None else None


def assign_token_to_user(token, user_id):
    request = """UPDATE judges_judge 
                 SET tel_id={} 
                 WHERE token='{}';""".format(user_id, token)
    execute(request)


"""
    USER DATA REQUESTS
"""


def fetch_user(user_id):
    request = """SELECT * 
                 FROM judges_judge 
                 WHERE tel_id={};""".format(user_id)
    return execute(request)


def get_current_state(user_id):
    request = """SELECT state 
                 FROM judges_judge 
                 WHERE tel_id={};""".format(user_id)
    resp = execute(request)
    return resp[0] if resp is not None else 0


def get_current_state_prefix(user_id):
    request = """SELECT state_prefix 
                 FROM judges_judge
                 WHERE tel_id={};""".format(user_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def fetch_username(user_id):
    request = """SELECT judges_judge.full_name 
                 FROM judges_judge 
                 WHERE tel_id={};""".format(user_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def get_judge_id(user_id):
    request = """SELECT judges_judge.judge_id
                 FROM judges_judge
                 WHERE tel_id={};""".format(user_id)
    resp = execute(request)
    return resp[0]


def set_user_lang(judge_id, new_lang):
    request = """UPDATE judges_judge
                 SET lang_id={},
                 lang_modified=true
                 WHERE tel_id={};""".format(new_lang, judge_id)
    execute(request)


def set_state(user_id, new_state):
    request = """UPDATE judges_judge 
                 SET state={} 
                 WHERE tel_id={};""".format(new_state, user_id)
    execute(request)


def set_state_prefix(user_id, new_prefix):
    request = """UPDATE judges_judge
                 SET state_prefix={}
                 WHERE tel_id={};""".format(new_prefix, user_id)
    execute(request)


def update_last_message(judge_id, new_last_msg):
    request = """UPDATE judges_judge SET last_msg={} WHERE tel_id={};""".format(new_last_msg, judge_id)
    execute(request)


def update_last_criterion(judge_id, new_last_criterion):
    request = """UPDATE judges_judge SET last_criterion={} WHERE tel_id={};""".format(new_last_criterion, judge_id)
    execute(request)


def fix_rate_scale(judge_id, project_id, new_rate_scale=0):
    data = fetch_assignment_rate_scale(judge_id, project_id)
    assignment_id, rate_scale = data[0], data[1]
    if rate_scale == 0:
        update_assignment_scale(assignment_id, new_rate_scale)


def fix_lang_id(judge_id, project_id, new_lang):
    data = fetch_assignment_lang_id(judge_id, project_id)
    assignment_id, lang_id = data[0], data[1]
    if lang_id is not None:
        update_assignment_lang_id(assignment_id, lang_id)
    else:
        lang_id = _get_default_lang_id()
        update_assignment_lang_id(assignment_id, lang_id)


def scale_preset_marks(criterion_id, assignment_id, new_rate_scale):
    request = """SELECT id, mark
                 FROM marks_mark
                 WHERE criterion_id = {}
                 AND assignment_id = {};""".format(criterion_id, assignment_id)
    data = execute(request)
    if criterion_read_only(criterion_id):
        request = """SELECT score
                     FROM criterions_criterion
                     WHERE id IN (
                         SELECT criterion_id
                         FROM marks_mark
                         WHERE id={}
                     );""".format(data[0])
        rate_scale = execute(request)[0]
        cur_mark = data[1]
        if cur_mark != 0 and rate_scale != 0:
            new_mark = new_rate_scale / (rate_scale / cur_mark)
        else:
            new_mark = 0
        return new_mark
    else:
        return data[1] if data is not None else None


def criterion_read_only(criterion_id):
    request = """SELECT read_only
                 FROM criterions_criterion
                 WHERE id={};""".format(criterion_id)
    return execute(request)[0]



"""
    TEAMS/PROJECT DATA REQUESTS
"""


def fetch_teams_for_judge(user_id):
    res_data = []
    today = datetime.datetime.isoformat(datetime.datetime.today())
    request = """SELECT projects_project.name, projects_project.id 
                 FROM projects_project 
                 WHERE projects_project.id 
                 IN (SELECT workloads_assignment.project_id 
                     FROM workloads_assignment 
                     WHERE workloads_assignment.workload_id 
                     IN (SELECT workloads_workload.id 
                         FROM workloads_workload 
                         WHERE workloads_workload.judge_id 
                         IN (SELECT judges_judge.id 
                             FROM judges_judge 
                             WHERE tel_id={})
                         AND workloads_workload.round_id
                         IN (SELECT workloads_round.id 
                             FROM workloads_round 
                             WHERE workloads_round.end_timestamp >= '{}' 
                             AND workloads_round.start_timestamp <= '{}')));""".format(user_id, today, today)
    team_data = execute(request, fetch_all=True)
    for cell in team_data:
        request = """SELECT COUNT(*) 
                     FROM workloads_assignment
                     WHERE workloads_assignment.project_id = {}
                     AND workloads_assignment.confirmed = true
                     AND workloads_assignment.workload_id 
                     IN (SELECT workloads_workload.id 
                         FROM workloads_workload 
                         WHERE workloads_workload.judge_id 
                         IN (SELECT judges_judge.id 
                             FROM judges_judge 
                             WHERE tel_id={}));""".format(cell[1], user_id)
        res_cell = (cell[0], cell[1], execute(request)[0])
        res_data.append(res_cell)
    return res_data


def fetch_teams_for_judge_by_round(judge_id, round_id):
    request = """SELECT projects_project.name, projects_project.id 
                 FROM projects_project 
                 WHERE projects_project.id 
                 IN (SELECT workloads_assignment.project_id 
                     FROM workloads_assignment 
                     WHERE workloads_assignment.workload_id 
                     IN (SELECT workloads_workload.id 
                         FROM workloads_workload 
                         WHERE workloads_workload.judge_id 
                         IN (SELECT judges_judge.id 
                             FROM judges_judge 
                             WHERE tel_id={})
                         AND workloads_workload.round_id={}
                         ));""".format(judge_id, round_id)
    return execute(request, fetch_all=True)


def fetch_team_names_by_ids(team_ids, user_id):
    res_data = []
    request = """SELECT projects_project.name, projects_project.id
                 FROM projects_project
                 WHERE projects_project.id
                 IN ({})""".format(",".join(team_ids))
    team_data = execute(request, fetch_all=True)
    for cell in team_data:
        request = """SELECT COUNT(*) 
                     FROM workloads_assignment
                     WHERE workloads_assignment.project_id = {}
                     AND workloads_assignment.confirmed = true
                     AND workloads_assignment.workload_id 
                     IN (SELECT workloads_workload.id 
                         FROM workloads_workload 
                         WHERE workloads_workload.judge_id 
                         IN (SELECT judges_judge.id 
                             FROM judges_judge 
                             WHERE tel_id={}));""".format(cell[1], user_id)
        res_cell = (cell[0], cell[1], execute(request)[0])
        res_data.append(res_cell)
    return res_data


def fetch_team_name_for_evaluation(judge_id):
    request = """SELECT id, name 
                 FROM projects_project 
                 WHERE id IN (
                       SELECT state_prefix 
                       FROM judges_judge 
                       WHERE tel_id={})""".format(judge_id)
    return execute(request)


def fetch_project_card_data(project_id):
    project_data = {
        "project_id": project_id,
        "project_name": None,
        "project_region": None,
        "project_title": None,
        "competition_id": None,
        "competition_name": None,
        "participants_names": []
    }
    request = """SELECT name, region, title, competition_id
                 FROM projects_project
                 WHERE projects_project.id={};""".format(project_id)
    data = execute(request)
    project_data["project_name"] = data[0]
    project_data["project_region"] = data[1]
    project_data["project_title"] = data[2]
    project_data["competition_id"] = data[3]
    request = """SELECT title
                 FROM projects_competition
                 WHERE id={};""".format(project_data["competition_id"])
    data = execute(request)
    project_data["competition_name"] = data[0]
    request = """SELECT name
                 FROM projects_participant
                 WHERE project_id={};""".format(project_data["project_id"])
    data = execute(request, fetch_all=True)
    data = [x[0] for x in data]
    project_data["participants_names"] = "\n".join(data)
    return project_data


"""
    ROUNDS DATA REQUESTS
"""


def fetch_round_title(round_id):
    request = """SELECT workloads_round.title
                 FROM workloads_round
                 WHERE workloads_round.id={};""".format(round_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def fetch_current_round():
    today = datetime.datetime.isoformat(datetime.datetime.today())
    request = """SELECT workloads_round.title
                 FROM workloads_round 
                 WHERE workloads_round.end_timestamp >= '{}' 
                 AND workloads_round.start_timestamp <= '{}';""".format(today, today)
    resp = execute(request)
    return resp[0] if resp is not None else None


def fetch_current_round_id():
    today = datetime.datetime.isoformat(datetime.datetime.today())
    request = """SELECT workloads_round.id
                 FROM workloads_round 
                 WHERE workloads_round.end_timestamp >= '{}' 
                 AND workloads_round.start_timestamp <= '{}';""".format(today, today)
    resp = execute(request);
    return resp[0] if resp is not None else None


"""
    MEDIA REQUESTS
"""


def media_exists(user_id, project_id):
    request = """SELECT id 
                 FROM judges_media
                 WHERE tel_id={}
                 AND project_id={};""".format(user_id, project_id)
    return bool(execute(request))


def create_media_row(user_id, project_id, round_id):
    if not media_exists(user_id, project_id):
        request = """INSERT INTO judges_media(tel_id, project_id, round_id)
                     VALUES ({}, {}, {});""".format(user_id, project_id, round_id)
        execute(request)


def set_photo_id(user_id, project_id, file_id, round_id):
    request = """UPDATE judges_media 
                 SET photo_id='{}' 
                 WHERE tel_id={} 
                 AND project_id={}
                 AND round_id={};""".format(file_id, user_id, project_id, round_id)
    execute(request)


def set_video_id(user_id, project_id, file_id, round_id):
    request = """UPDATE judges_media 
                     SET video_id='{}' 
                     WHERE tel_id={} 
                     AND project_id={}
                     AND round_id={};""".format(file_id, user_id, project_id, round_id)
    execute(request)


def set_voice_id(user_id, project_id, file_id, round_id):
    request = """UPDATE judges_media 
                 SET voice_id='{}' 
                 WHERE tel_id={} 
                 AND project_id={}
                 AND round_id={};""".format(file_id, user_id, project_id, round_id)
    execute(request)


def get_photo_id(user_id, project_id, round_id):
    request = """SELECT photo_id 
                 FROM judges_media
                 WHERE tel_id={} 
                 AND project_id={}
                 AND round_id={};""".format(user_id, project_id, round_id)
    resp = execute(request)
    return resp[0] if resp else None


def get_video_id(user_id, project_id, round_id):
    request = """SELECT video_id 
                 FROM judges_media
                 WHERE tel_id={} 
                 AND project_id={}
                 AND round_id={};""".format(user_id, project_id, round_id)
    resp = execute(request)
    return resp[0] if resp else None


def get_voice_id(user_id, project_id, round_id):
    request = """SELECT voice_id 
                 FROM judges_media
                 WHERE tel_id={} 
                 AND project_id={}
                 AND round_id={};""".format(user_id, project_id, round_id)
    resp = execute(request)
    return resp[0] if resp else None


"""
    CRITERION REQUESTS
"""


def criterions_for_judge_exists(judge_id):
    language = get_usr_lang_id(judge_id)
    request = """SELECT COUNT(id) 
                 FROM criterions_criteriongroup 
                 WHERE lang_id={};""".format(language)
    return bool(execute(request)[0])


def fetch_criterions(judge_id, lang_id=None):
    if lang_id:
        language = lang_id
    else:
        if criterions_for_judge_exists(judge_id):
            language = get_usr_lang_id(judge_id)
        else:
            language = _get_default_lang_id()
    request = """SELECT id, criterion_order, criterion_group_id, title, description, read_only  
                 FROM criterions_criterion 
                 WHERE criterion_group_id 
                 IN (SELECT id 
                     FROM criterions_criteriongroup 
                     WHERE lang_id={})
                 ORDER BY id ASC;""".format(language)
    return execute(request, fetch_all=True)


def fetch_last_message(judge_id):
    request = """SELECT last_msg FROM judges_judge WHERE tel_id={}""".format(judge_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def fetch_criterion_by_id(judge_id, criterion_number):
    result = []
    criterion = fetch_criterions(judge_id)
    for i in range(-1, 2):
        if criterion_number + i < 0 or criterion_number + i >= len(criterion):
            result.append(None)
        else:
            result.append(criterion[criterion_number + i])
    return result, len(criterion)


def fetch_last_criterion_id(judge_id):
    request = """SELECT last_criterion 
                 FROM judges_judge 
                 WHERE tel_id={}""".format(judge_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def fetch_rate_scale(judge_id):
    request = """SELECT rate_scale FROM judges_judge WHERE tel_id={}""".format(judge_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def fetch_assignment_rate_scale(judge_id, project_id):
    round = fetch_current_round_id()
    request = """SELECT workloads_assignment.id, workloads_assignment.rate_scale 
                 FROM workloads_assignment
                 WHERE workloads_assignment.project_id={}
                 AND workloads_assignment.workload_id IN (
                     SELECT workloads_workload.id
                     FROM workloads_workload
                     WHERE workloads_workload.judge_id IN (
                        SELECT judges_judge.id 
                        FROM judges_judge
                        WHERE judges_judge.tel_id={})
                     AND workloads_workload.round_id={} 
                 );""".format(project_id, judge_id, round)
    return execute(request)


def fetch_assignment_rate_scale_by_id(assignment_id):
    request = """SELECT workloads_assignment.id, workloads_assignment.rate_scale 
                 FROM workloads_assignment
                 WHERE id={};""".format(assignment_id)
    return execute(request)


def fetch_assignment_lang_id(judge_id, project_id):
    round = fetch_current_round_id()
    request = """SELECT workloads_assignment.id, workloads_assignment.lang_id 
                 FROM workloads_assignment
                 WHERE workloads_assignment.project_id={}
                 AND workloads_assignment.workload_id IN (
                     SELECT workloads_workload.id
                     FROM workloads_workload
                     WHERE workloads_workload.judge_id IN (
                        SELECT judges_judge.id 
                        FROM judges_judge
                        WHERE judges_judge.tel_id={})
                     AND workloads_workload.round_id={} 
                 );""".format(project_id, judge_id, round)
    return execute(request)


def update_assignment_scale(assignment_id, new_rate_scale):
    request = """UPDATE workloads_assignment
                 SET rate_scale={}
                 WHERE workloads_assignment.id={};""".format(new_rate_scale, assignment_id)
    execute(request)


def update_assignment_lang_id(assignment_id, new_lang_id):
    request = """UPDATE workloads_assignment
                 SET lang_id={}
                 WHERE workloads_assignment.id={};""".format(new_lang_id, assignment_id)
    execute(request)


def fetch_last_criterion_full_id(judge_id, criterion_id=None):
    criterion_id = fetch_last_criterion_id(judge_id=judge_id)
    if criterion_id is None:
        criterion_id = 0
    criterion = fetch_criterions(judge_id=judge_id)
    result = criterion[criterion_id]
    return result[0] if len(result) > 0 else None


def fetch_workload(judge_id, round_id=None):
    request = """SELECT id 
                 FROM workloads_workload 
                 WHERE judge_id 
                 IN (SELECT id 
                     FROM judges_judge 
                     WHERE tel_id={}) 
                 AND round_id={}""".format(
        judge_id,
        fetch_current_round_id() if round_id is None else round_id)
    resp = execute(request);
    return resp[0] if resp is not None else None


def fetch_assignment(project_id, workload_id):
    request = """SELECT id 
                 FROM workloads_assignment 
                 WHERE project_id={} 
                 AND workload_id={}""".format(project_id, workload_id)
    resp = execute(request);
    return resp[0] if resp is not None else None


"""
    MARKS REQUESTS
"""


def set_mark_in_assign(assignment_id, criterion_id, score):
    request = """UPDATE marks_mark SET mark={} WHERE assignment_id={} AND criterion_id={}; 
                 INSERT INTO marks_mark (assignment_id, criterion_id, mark) SELECT {}, {}, {} 
                 WHERE NOT EXISTS (SELECT 1 FROM marks_mark WHERE assignment_id={} AND criterion_id={});""".format(
        score, assignment_id, criterion_id, assignment_id, criterion_id, score, assignment_id, criterion_id)
    execute(request)


def fetch_mark(assignment_id, criterion_id):
    request = """SELECT mark 
                 FROM marks_mark 
                 WHERE assignment_id={} 
                 AND criterion_id={}""".format(assignment_id, criterion_id)
    resp = execute(request)
    return resp[0] if resp is not None else resp


def get_judge_rate_scale(judge_id):
    request = """SELECT rate_scale
                 FROM judges_judge
                 WHERE tel_id={};""".format(judge_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def set_user_scale(judge_id, new_scale):
    request = """UPDATE judges_judge
                 SET rate_scale={}
                 WHERE tel_id={};""".format(new_scale, judge_id)
    execute(request)


def activate_evaluation_mode(judge_id, project_id):
    fix_rate_scale(judge_id, project_id, new_rate_scale=get_judge_rate_scale(judge_id))
    fix_lang_id(judge_id, project_id, new_lang=get_usr_lang_id(judge_id))
    set_state(judge_id, settings.State.EVALUATION.value)
    set_state_prefix(judge_id, project_id)
    update_last_criterion(judge_id, 0)


def get_mark(judge_id, round_id=None, archive=False):
    project_id = get_current_state_prefix(judge_id)
    criterion_id = fetch_last_criterion_full_id(judge_id)
    assignment_id = get_assignment(judge_id, project_id=project_id, round_id=round_id)
    if assignment_id:
        rate_scale = fetch_assignment_rate_scale_by_id(assignment_id)
    else:
        rate_scale = fetch_assignment_rate_scale(judge_id, project_id)
    if archive and rate_scale[1] == 0:
        mark = fetch_mark(assignment_id, criterion_id)
    else:
        mark = scale_preset_marks(criterion_id, assignment_id, rate_scale[1])
    return mark


def get_assignment(judge_id, project_id, round_id=None):
    workload_id = fetch_workload(judge_id, round_id)
    assignment_id = fetch_assignment(project_id, workload_id)
    return assignment_id


def set_mark(judge_id, project_id, score):
    criterion_id = fetch_last_criterion_full_id(judge_id=judge_id)
    assignment_id = get_assignment(judge_id, project_id)
    set_mark_in_assign(assignment_id=assignment_id, criterion_id=criterion_id, score=score)


def fetch_rounds(judge_id):
    request = """SELECT id, title, round_id
                  FROM workloads_round 
                  WHERE id IN (
                    SELECT round_id 
                    FROM workloads_workload
                    WHERE judge_id IN (
                      SELECT id 
                      FROM judges_judge
                      WHERE tel_id={}))
                  ;""".format(judge_id)
    rounds = execute(request, fetch_all=True)
    return sorted(rounds, key=lambda x: x[2])


def project_could_be_evaluated(judge_id, project_id):
    round = fetch_current_round_id()
    if round is None:
        return 2
    request = """SELECT modified, last_modified_ts, confirmed 
                     FROM workloads_assignment
                     WHERE workloads_assignment.project_id={}
                     AND workloads_assignment.workload_id IN (
                         SELECT workloads_workload.id
                         FROM workloads_workload
                         WHERE workloads_workload.judge_id IN (
                            SELECT judges_judge.id 
                            FROM judges_judge
                            WHERE judges_judge.tel_id={})
                         AND workloads_workload.round_id={}
                     );""".format(project_id, judge_id, round)
    resp = execute(request)
    modified, last_modified, confirmed = resp
    fits_time = False
    if last_modified is not None:
        dt = datetime.datetime.today()
        dt = dt.replace(tzinfo=None)
        tm = last_modified.replace(tzinfo=None)
        fits_time = (tm + datetime.timedelta(minutes=settings.MIN_TO_MODIFY)) > dt
    if confirmed is False and modified is False:
        return 0
    elif confirmed is True and fits_time and modified is False:
        return 1
    else:
        return 2


def get_project_title_by_id(project_id):
    request = """SELECT title
                 FROM projects_project
                 WHERE id={};""".format(project_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def all_marks_set(judge_id, project_id):
    assignment_id, __ = fetch_assignment_rate_scale(judge_id, project_id)
    request = """SELECT COUNT(*) 
                 FROM marks_mark
                 WHERE assignment_id={};""".format(assignment_id)
    criterion_number = len(fetch_criterions(judge_id))
    resp = execute(request)[0]
    if resp >= criterion_number:
        return True
    return False


def fetch_first_unset_criteria_id(judge_id, project_id):
    assignment_id, __ = fetch_assignment_rate_scale(judge_id, project_id)
    lang = get_usr_lang_id(judge_id)
    request = """SELECT criterions_criterion.id 
                 FROM criterions_criterion
                 WHERE criterions_criterion.id NOT IN (
                      SELECT marks_mark.criterion_id
                      FROM marks_mark
                      WHERE marks_mark.assignment_id={})
                 AND criterions_criterion.lang_id={};""".format(assignment_id, lang)
    resp = execute(request, fetch_all=True)
    if resp is None or len(resp) < 1:
        return None
    resp.sort(key=lambda x: x[0])
    criterion_id = resp[0][0]
    criterion_ids = [x[0] for x in fetch_criterions(judge_id)]
    return criterion_ids.index(criterion_id)


def confirm_finish(judge_id, project_id):
    assignment_id, __ = fetch_assignment_rate_scale(judge_id, project_id)
    request = """UPDATE workloads_assignment
                 SET confirmed=true, last_modified_ts='{}'
                 WHERE id={};""".format(datetime.datetime.now(), assignment_id)
    execute(request)


def set_modified(judge_id, project_id):
    assignment_id, __ = fetch_assignment_rate_scale(judge_id, project_id)
    request = """UPDATE workloads_assignment
                 SET modified=true, last_modified_ts='{}'
                 WHERE id={};""".format(datetime.datetime.now(), assignment_id)
    execute(request)


def set_current_round(judge_id, current_round):
    request = """UPDATE judges_judge
                 SET current_round_id={}
                 WHERE tel_id={};""".format(current_round, judge_id)
    execute(request)


def get_current_round(judge_id):
    request = """SELECT current_round_id
                 FROM judges_judge
                 WHERE tel_id={};""".format(judge_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def fetch_criterion_group_title_by_id(group_id):
    request = """SELECT title
                 FROM criterions_criteriongroup
                 WHERE id={};""".format(group_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


def project_was_evaluated(judge_id, project_id, round_id):
    request = """SELECT confirmed
                 FROM workloads_assignment
                 WHERE project_id={}
                 AND workload_id IN (
                    SELECT id
                    FROM workloads_workload
                    WHERE round_id={}
                    AND judge_id IN (
                        SELECT id
                        FROM judges_judge
                        WHERE tel_id={}
                    )
                 );""".format(project_id, round_id, judge_id)
    resp = execute(request)
    return resp[0] if resp is not None else None


"""
    ADMIN COMMANDS
"""


def _drop_token(token):
    """
        Drops the current user from token.
    :param token:
    :return:
    """
    request = """UPDATE judges_judge 
                 SET tel_id=0, state=0 
                 WHERE token='{}';""".format(token)
    execute(request)


if __name__ == "__main__":
    pass
