#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import logging
from enum import Enum

# BOT
# Telegram token of your bot from @BotFather
TOKEN = "your_bot_token"

# Default language for all of the users
DEF_LANG = "ru"

# Amount of time that determines the period for mark modification
MIN_TO_MODIFY = 10

# PROXY
USE_PROXY = False
PROXY = {
    "addr": "proxy_ip",
    "port": 1488,
    "username": "proxy_username",
    "password": "proxy_password"
}

WEBHOOK_HOST = 'webhook_ip'
WEBHOOK_PORT = 443
WEBHOOK_LISTEN = '0.0.0.0'

# WEBHOOK
WEBHOOK_SSL_CERT = './webhook_cert.pem'
WEBHOOK_SSL_PRIV = './webhook_pkey.pem'
WEBHOOK_URL_BASE = "https://%s:%s" % (WEBHOOK_HOST, WEBHOOK_PORT)
WEBHOOK_URL_PATH = f"/{TOKEN}/"

# PSG
POSTGRESQL = {"dbname": "pg_db_name",
              "user": "pg_db_user",
              "password": "pg_db_password",
              "host": "postgres",
              "port": 5432
              }

# RMQ
RMQ = "rmq"
RMQ_USER = "rmq_user"
RMQ_PASS = "rmq_password"

LOG_FORMAT = ('%(levelname) -10s %(asctime)s %(name) -30s %(funcName) -35s %(lineno) -5d: %(message)s')
LOG = True

CMD_MENU_SET = [
    "c_str",  # cmd_select_team_range
    "c_spc",  # cmd_show_project_card
    "c_fe",  # cmd_finish_eval
    "c_se",  # cmd_start_eval
    "c_de"  # cmd_decline_eval
]
CMD_EVALUATION_SET = [
    "c_se",  # cmd_start_eval
    "c_cm",  # dms_confirm_modify
    "c_en",  # cmd_evaluate_next
    "c_ep",  # cmd_evaluate_prev
    "c_ssc",  # cmd_set_score
    "c_fe"  # cmd_finish_eval
]
CMD_ARCHIVE_SET = [
    "c_sa",  # cmd_show_archive
    "c_ap",  # cmd_archive_prev
    "c_an",  # cmd_archive_next
    "c_sr",  # cmd_select_round
    "c_fa",  # cmd_finish_archive
    "c_sac",  # cmd_show_archive_card
    "c_str"  # cmd_select_team_range
]
CMD_SETTINGS_SET = [
    "c_sse",  # cmd_scale_settings
    "c_lse",  # cmd_lang_settings
    "c_ssc",  # cmd_set_scale
    "c_sl",  # cmd_set_lang
    "c_sm"  # cmd_send_menu
]
CMD_MEDIA_SET = [
    "c_gv",  # cmd_get_voice
    "c_gp",  # cmd_get_photo
    "c_gvi"  # cmd_get_video
]


class State(Enum):
    NULL = 0  # Null state. Returned from db when user not found
    TK_WAIT = 4  # Start Regular (Token Request)
    TK_CHECK = 6  # Token Check
    TK_WRONG = 8  # Token Checked Request Wrong (Token wrong -> request New Token)
    MENU = 10  # Initial state after token validated
    ARCHIVE = 12  # Archive
    SETTINGS = 13  # Settings
    EVALUATION = 20  # This state is required for evaluation, but the settings are disabled


def create_logger(logger_name: str = 'bot'):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.INFO)

    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)

    formatter = logging.Formatter(LOG_FORMAT)
    ch.setFormatter(formatter)

    logger.addHandler(ch)

    return logger
