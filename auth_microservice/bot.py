#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import pickle
import logging

from telebot import TeleBot, apihelper, types, logger

from core import markup
from core import settings as s
from core import database as db
from core.consumer import PickleConsumer


if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    bot.process_new_messages([body["message"]])


con = PickleConsumer(s.RMQ, 5672, 'auth', callback, exchange='rate_bot', logger='auth', username=s.RMQ_USER, password=s.RMQ_PASS)


def extract_token(text):
    # Extracts token from the /start command.
    return text.split()[1] if len(text.split()) > 1 else None


@bot.message_handler(commands=["start"])
def start(message):
    """
        Checks tokens and judge states.

    `If two users have the same token the last who used it to log in would be considered as an active.`
    `Users can't log out once they log in. To do that use database._drop_token`
    """
    j_id = message.from_user.id
    token = extract_token(message.text)
    if db.get_current_state(j_id) >= s.State.MENU.value:
        db.set_state(j_id, new_state=s.State.MENU.value)
        send_previously_logged_in(j_id)
        notify_menu_service(message)
    elif token and db.token_exists(token):
        db.assign_token_to_user(token, j_id)
        db.set_state(j_id, new_state=s.State.MENU.value)
        send_welcome(j_id)
        notify_menu_service(message)
    elif token and not db.token_exists(token):
        msg_wrong_token = db.fetch_labels(s.DEF_LANG, "msg_wrong_token")
        bot.send_message(chat_id=j_id,
                         parse_mode="markdown",
                         text=msg_wrong_token)
    else:
        msg_token_request = db.fetch_labels(s.DEF_LANG, "msg_token_request")
        bot.send_message(chat_id=j_id,
                         parse_mode="markdown",
                         text=msg_token_request)


@bot.message_handler(content_types=["text"])
def message_handler(message):
    """
        Checks token
    """
    j_id = message.from_user.id
    if message.text and db.token_exists(message.text):
        db.assign_token_to_user(message.text, j_id)
        send_welcome(j_id)
        notify_menu_service(message)
        db.set_state(j_id, new_state=s.State.MENU.value)
    else:
        msg_wrong_token = db.fetch_labels(s.DEF_LANG, "msg_wrong_token")
        bot.send_message(chat_id=j_id,
                         parse_mode="markdown",
                         text=msg_wrong_token)


def send_previously_logged_in(judge_id):
    """
        Sends message to a user if he is already logged in
    """
    msg_already_logged_in = db.fetch_labels(db.get_usr_lang(judge_id), "msg_already_logged_in")[0]
    user_name = db.fetch_username(judge_id)
    bot.send_message(chat_id=judge_id,
                     parse_mode="markdown",
                     text=msg_already_logged_in.format(user_name),
                     reply_markup=markup.MAIN_MENU_MARKUP)


def send_welcome(judge_id):
    """
        Sends message to a user if he is already logged in
    """
    msg_welcome = db.fetch_labels(db.get_usr_lang(judge_id), "msg_welcome")[0]
    user_name = db.fetch_username(judge_id)
    judge_lang = db.get_full_lang_name(db.get_usr_lang(judge_id))
    msg_welcome = msg_welcome.format(user_name, judge_lang)
    bot.send_message(chat_id=judge_id,
                     parse_mode="markdown",
                     text=msg_welcome,
                     reply_markup=markup.MAIN_MENU_MARKUP)


def notify_menu_service(message):
    """
        Sends the notification to a menu service to send team_select
        See main_menu.send_team_select
    """
    body = pickle.dumps({
            "command": "send_team_select",
            "message": message,
            "call": None
        }
    )
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="main_menu",
        body=body
    )


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[AUTH] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()

