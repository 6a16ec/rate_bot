FROM python:3.6.5

RUN mkdir /src/
WORKDIR /src/

EXPOSE 443
EXPOSE 8443

ADD ./requirements.txt /src/requirements.txt
RUN pip install --upgrade pip && pip install -r requirements.txt

ADD ./webhook_cert.pem /src/webhook_cert.pem
ADD ./webhook_pkey.pem /src/webhook_pkey.pem

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./webhook_receiver/ /src/webhook_receiver/
CMD [ "python", "./webhook_receiver/main_webhook.py" ]
