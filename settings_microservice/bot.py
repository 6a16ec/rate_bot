#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import re
import pickle
import logging

from telebot import TeleBot, apihelper, logger

from core import database as db
from core import markup as mrk
from core import settings as s
from core.consumer import PickleConsumer


if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    if body["call"]:
        bot.process_new_callback_query([body["call"]])
    else:
        bot.process_new_messages([body["message"]])


con = PickleConsumer(s.RMQ, 5672, 'settings', callback, exchange='rate_bot', logger='settings', username=s.RMQ_USER, password=s.RMQ_PASS)


@bot.message_handler(content_types=["text"])
def settings_handler(message):
    judge_id = message.from_user.id
    if message.text == mrk.B_SETTINGS or message.text == mrk.B_REFRESH:
        send_settings_menu(judge_id)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    judge_id = call.from_user.id
    command = re.search('(?<=/)\w+', call.data).group(0)
    if command == "c_sm":
        bot.answer_callback_query(call.id)
        send_settings_menu(judge_id, update=True)
    elif command == "c_sse":
        bot.answer_callback_query(call.id)
        send_settings_scale(judge_id, update=True)
    elif command == "c_lse":
        bot.answer_callback_query(call.id)
        send_settings_lang(judge_id, update=True)
    elif command == "c_ssc":
        new_scale = int(re.search('(?<=-v.)\w+', call.data).group(0))
        db.set_user_scale(judge_id, new_scale)
        bot.answer_callback_query(
            callback_query_id=call.id,
            text="\U0001F4BE"
        )
        send_settings_scale(judge_id, update=True)
    elif command == "c_sl":
        new_lang = int(re.search('(?<=-v.)\w+', call.data).group(0))
        db.set_user_lang(judge_id, new_lang)
        bot.answer_callback_query(
            callback_query_id=call.id,
            text="\U0001F4BE"
        )
        send_settings_lang(judge_id, update=True)


def send_settings_menu(judge_id, update=False):
    lang = db.get_usr_lang(judge_id)
    db.set_state(judge_id, new_state=s.State.SETTINGS.value)
    rate_scale = db.get_judge_rate_scale(judge_id)
    full_lang = db.get_full_lang_name(lang)
    text_settings, text_scale, text_lang = db.fetch_labels(lang, "settings", "rate_scale", "usr_language")
    msg_text = "*{}*\n{}: 0..{}\n{}: {}".format(
        text_settings, text_scale, rate_scale, text_lang, full_lang.capitalize()
    )
    settings_markup = mrk.create_settings_mrk(text_scale, text_lang)
    if update:
        last_msg = db.fetch_last_message(judge_id)
        bot.edit_message_text(
            chat_id=judge_id,
            message_id=last_msg,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=settings_markup
        )
    else:
        last_msg = bot.send_message(
            judge_id,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=settings_markup
        )
        db.update_last_message(judge_id, last_msg.message_id)


def send_settings_scale(judge_id, update=False):
    lang = db.get_usr_lang(judge_id)
    text_scale = db.fetch_labels(lang, "rate_scale")[0]
    rate_scale = db.get_judge_rate_scale(judge_id)
    msg_text = "*{}* 0..{}".format(text_scale, rate_scale)
    if update:
        last_msg = db.fetch_last_message(judge_id)
        bot.edit_message_text(
            chat_id=judge_id,
            message_id=last_msg,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=mrk.SCALE_MARKUP
        )
    else:
        last_msg = bot.send_message(
            judge_id,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=mrk.SCALE_MARKUP
        )
        db.update_last_message(judge_id, last_msg.message_id)


def send_settings_lang(judge_id, update=False):
    lang = db.get_usr_lang(judge_id)
    text_lang = db.fetch_labels(lang, "usr_language")[0]
    full_lang = db.get_full_lang_name(lang)
    msg_text = "*{}*: {}".format(text_lang, full_lang.capitalize())
    if update:
        last_msg = db.fetch_last_message(judge_id)
        bot.edit_message_text(
            chat_id=judge_id,
            message_id=last_msg,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=mrk.language_select_mrk()
        )
    else:
        last_msg = bot.send_message(
            judge_id,
            text=msg_text,
            parse_mode="markdown",
            reply_markup=mrk.language_select_mrk()
        )
        db.update_last_message(judge_id, last_msg.message_id)


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[SETTINGS] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()

