#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import re
import pickle
import logging

from telebot import TeleBot, apihelper, logger

from core import database as db
from core import markup as mrk
from core import settings as s
from core.consumer import PickleConsumer

if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    if body["command"] == "send_team_select":
        if body["call"]:
            judge_id = body["call"].from_user.id
        else:
            judge_id = body["message"].from_user.id
        db.set_state(judge_id, new_state=s.State.MENU.value)
        send_team_select(judge_id)
    elif body["command"] == "send_project_card":
        judge_id = body["call"].from_user.id
        project_id, round_id = body["args"]
        db.set_state(judge_id, new_state=s.State.MENU.value)
        db.set_state_prefix(judge_id, project_id)
        db.set_current_round(judge_id, round_id)
        send_project_card(judge_id, project_id)
    elif body["call"]:
        bot.process_new_callback_query([body["call"]])
    else:
        bot.process_new_messages([body["message"]])


con = PickleConsumer(s.RMQ, 5672, 'main_menu', callback, exchange='rate_bot', logger='main_menu', username=s.RMQ_USER, password=s.RMQ_PASS)


@bot.message_handler(content_types=["text"])
def main_menu_handler(message):
    judge_id = message.from_user.id
    if message.text == mrk.B_SEARCH:
        db.set_state(judge_id, new_state=s.State.MENU.value)
        send_team_select(judge_id)
    elif message.text == mrk.B_REFRESH:
        send_team_select(judge_id)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    judge_id = call.from_user.id
    command = re.search('(?<=/)\w+', call.data).group(0)
    if command == "c_spc":
        db.set_current_round(judge_id, db.fetch_current_round_id())
        bot.answer_callback_query(call.id)
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        send_project_card(judge_id, project_id)
    elif command == "c_str":
        lang = db.get_usr_lang(judge_id)
        bot.answer_callback_query(call.id)
        msg_team_select = db.fetch_labels(lang, "msg_team_select")[0]
        team_ids = re.findall('\d+', call.data)
        teams_data = db.fetch_team_names_by_ids(team_ids, judge_id)
        mrk_team_response = mrk.get_detailed_team_select_keyboard(teams_data)
        msg_text = msg_team_select.format(mrk.B_SEARCH, db.fetch_current_round(), "")
        bot.send_message(chat_id=judge_id,
                         text=msg_text,
                         reply_markup=mrk_team_response["markup"],
                         parse_mode="markdown")
    elif command == "c_se":
        project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
        eval_status = db.project_could_be_evaluated(judge_id, project_id)
        bot.answer_callback_query(call.id)
        if eval_status == 0:
            send_confirmation(judge_id, project_id)
        elif eval_status == 1:
            send_confirm_modification(judge_id, project_id)
        else:
            lang = db.get_usr_lang(judge_id)
            msg_you_cant_modify = db.fetch_labels(lang, "msg_you_cant_modify_anymore")
            bot.answer_callback_query(call.id, text=msg_you_cant_modify, show_alert=True)
            db.set_state(judge_id, s.State.ARCHIVE.value)
            db.set_current_round(judge_id, db.get_current_round(judge_id))
            notify_archive(call, project_id)
    elif command == "c_de":
        bot.answer_callback_query(call.id)
        send_team_select(judge_id)


def send_project_card(user_id, project_id):
    lang = db.get_usr_lang(user_id)
    msg_project_card, butt_go_to_marks = db.fetch_labels(lang, "msg_project_card", "but_go_to_marks")
    project_data = db.fetch_project_card_data(project_id)
    round_id = db.get_current_round(user_id)
    project_card_markup = mrk.get_project_card_inline_keyboard(project_id, butt_go_to_marks, round_id=round_id)
    bot.send_message(chat_id=user_id,
                     text=msg_project_card.format(**project_data),
                     reply_markup=project_card_markup,
                     parse_mode="markdown")


def send_team_select(user_id):
    lang = db.get_usr_lang(user_id)
    msg_team_select = db.fetch_labels(lang, "msg_team_select")[0]
    teams = db.fetch_teams_for_judge(user_id)
    if teams:
        mrk_team_response = mrk.get_detailed_team_select_keyboard(teams)
        if mrk_team_response["small"]:
            msg_text = msg_team_select.format(mrk.B_SEARCH, db.fetch_current_round(), "")
        else:
            msg_text = msg_team_select.format(mrk.B_SEARCH, db.fetch_current_round(), mrk_team_response["description"])
        bot.send_message(user_id,
                         text=msg_text,
                         parse_mode="markdown",
                         reply_markup=mrk_team_response["markup"])
    else:
        msg_no_available_teams = db.fetch_labels(lang, "msg_no_available_teams")[0]
        bot.send_message(user_id,
                         parse_mode="markdown",
                         text=msg_no_available_teams,
                         reply_markup=mrk.MAIN_MENU_MARKUP)


def send_confirmation(judge_id, project_id):
    lang = db.get_usr_lang(judge_id)
    msg_confirm_eval_start, b_conf, b_dec = db.fetch_labels(lang, "msg_confirm_eval_start",
                                                            "but_confirm", "but_decline")
    markup = mrk.create_confirm_markup(project_id, b_conf, b_dec)
    msg = bot.send_message(judge_id,
                           msg_confirm_eval_start,
                           reply_markup=markup,
                           parse_mode="markdown")
    db.update_last_message(judge_id, msg.message_id)


def send_confirm_modification(judge_id, project_id):
    lang = db.get_usr_lang(judge_id)
    msg_confirm_eval_start, b_conf, b_dec = db.fetch_labels(lang, "msg_confirm_modif_start",
                                                            "but_confirm", "but_decline")
    markup = mrk.create_confirm_modif_markup(project_id, b_conf, b_dec)
    msg = bot.send_message(judge_id,
                           msg_confirm_eval_start,
                           reply_markup=markup,
                           parse_mode="markdown")
    db.update_last_message(judge_id, msg.message_id)


def notify_archive(call, team_id):
    body = pickle.dumps(
        {
            "command": "show_archive -t:{}".format(team_id),
            "message": None,
            "call": call
        }
    )
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="archive",
        body=body
    )


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[MAIN_MENU] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()

