FROM python:3.6.5

RUN mkdir /src/
WORKDIR /src/

ADD ./requirements.txt /src/requirements.txt
RUN pip install --upgrade pip && pip install -r requirements.txt

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./main_menu/ /src/main_menu/
CMD [ "python", "./main_menu/bot.py" ]
