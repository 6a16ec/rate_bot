#!/usr/bin/python3.6
# -*- coding: utf-8 -*-

import re
import pickle
import logging

from telebot import TeleBot, apihelper, types, logger

from core import settings as s
from core import database as db
from core import markup as mrk
from core.consumer import PickleConsumer


if s.USE_PROXY:
    apihelper.proxy = {"https": "socks5://{username}:{password}@{addr}:{port}".format(**s.PROXY)}

bot = TeleBot(s.TOKEN)


def callback(consumer, body):
    if body["call"]:
        bot.process_new_callback_query([body["call"]])
    else:
        bot.process_new_messages([body["message"]])


con = PickleConsumer(s.RMQ, 5672, 'media', callback, exchange='rate_bot', logger='media', username=s.RMQ_USER, password=s.RMQ_PASS)


@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    lang = db.get_usr_lang(call.from_user.id)
    command = re.search('(?<=/)\w+', call.data).group(0)
    project_id = int(re.search('(?<=-t.)\w+', call.data).group(0))
    round_id = int(re.search('(?<=-r.)\w+', call.data).group(0))
    msg_no_voice, msg_no_photo, msg_no_video = db.fetch_labels(lang, "msg_no_voice", "msg_no_photo", "msg_no_video")
    if command == "c_gv":
        file_id = db.get_voice_id(call.from_user.id, project_id, round_id)
        if file_id:
            bot.send_voice(call.from_user.id, file_id)
            if db.get_current_state(call.from_user.id) == 12:
                notify_archive_service(call, project_id, round_id)
            else:
                notify_menu_service(call, project_id, round_id)
        else:
            bot.answer_callback_query(call.id, msg_no_voice)
    elif command == "c_gp":
        file_id = db.get_photo_id(call.from_user.id, project_id, round_id)
        if file_id:
            bot.send_photo(call.from_user.id, file_id)
            if db.get_current_state(call.from_user.id) == 12:
                notify_archive_service(call, project_id, round_id)
            else:
                notify_menu_service(call, project_id, round_id)
        else:
            bot.answer_callback_query(call.id, msg_no_photo)
    elif command == "c_gvi":
        file_id = db.get_video_id(call.from_user.id, project_id, round_id)
        if file_id:
            bot.send_video(call.from_user.id, file_id)
            notify_menu_service(call, project_id, round_id)
        else:
            bot.answer_callback_query(call.id, msg_no_video)


@bot.message_handler(content_types=["voice", "video", "photo"])
def handle_media(message):
    project_id = db.get_current_state_prefix(message.from_user.id)
    round_id = db.get_current_round(message.from_user.id)
    notify_eval_service(message)
    db.create_media_row(message.from_user.id, project_id, round_id)
    if message.content_type == "photo":
        file_id = message.photo[-1].file_id
        db.set_photo_id(message.from_user.id, project_id, file_id, round_id)
    elif message.content_type == "voice":
        file_id = message.voice.file_id
        db.set_voice_id(message.from_user.id, project_id, file_id, round_id)
    else:
        file_id = message.video.file_id
        db.set_video_id(message.from_user.id, project_id, file_id, round_id)


def notify_eval_service(message):
    body = pickle.dumps({
            "command": "resend_evaluation",
            "message": message,
            "call": None
        }
    )
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="evaluation",
        body=body
    )


def notify_menu_service(call, project_id, round_id):
    body = pickle.dumps({
        "command": "send_project_card",
        "args": [project_id, round_id],
        "message": None,
        "call": call
    })
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="main_menu",
        body=body
    )


def notify_archive_service(call, project_id, round_id):
    body = pickle.dumps({
        "command": "send_project_card",
        "args": [project_id, round_id],
        "message": None,
        "call": call
    })
    con._channel.basic_publish(
        exchange="rate_bot",
        routing_key="archive",
        body=body
    )


if __name__ == '__main__':
    try:
        logger.log(logging.DEBUG, '[MEDIA] Waiting for messages. To exit press CTRL+C')
        con.run()
    except KeyboardInterrupt:
        con.stop()
