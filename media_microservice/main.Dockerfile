FROM python:3.6.5

RUN mkdir /src/
WORKDIR /src/

ADD ./requirements.txt /src/requirements.txt
RUN pip install --upgrade pip && pip install -r requirements.txt

ADD  ./core/ /src/core/
ENV	PYTHONPATH="$PYTHONPATH:/src"

ADD  ./media_microservice/ /src/media_microservice/
CMD [ "python", "./media_microservice/bot.py" ]
